# Castle

_A 3D Castle Chess Piece._

[![License](https://img.shields.io/badge/license-0BSD-black?style=flat-square)][0BSD]

## License

SPDX-License-Identifier: [0BSD][0BSD]

[0BSD]: https://spdx.org/licenses/0BSD.html
